import 'package:flutter/material.dart';

const LOCALES = [
  Locale('en', 'US'),
  Locale('vi', 'VN'),
];