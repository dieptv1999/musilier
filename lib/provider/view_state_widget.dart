import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musilier/config/resource_manager.dart';
import 'package:lottie/lottie.dart';

import 'view_state.dart';

class ViewStateBusyWidget extends StatelessWidget {
  String size = 'small';

  ViewStateBusyWidget({this.size = 'small'});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: size == 'small'
            ? CircularProgressIndicator()
            : Lottie.asset('assets/lottie/cloud-music.json', height: 80));
  }
}

class ViewStateWidget extends StatelessWidget {
  final String title;
  final String message;
  final Widget image;
  final Widget buttonText;
  final String buttonTextData;
  final VoidCallback onPressed;

  ViewStateWidget(
      {required Key key,
      required this.image,
      required this.title,
      required this.message,
      required this.buttonText,
      required this.onPressed,
      required this.buttonTextData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var titleStyle =
        Theme.of(context).textTheme.subhead!.copyWith(color: Colors.grey);
    var messageStyle = titleStyle.copyWith(
        color: titleStyle.color!.withOpacity(0.7), fontSize: 14);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        image,
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                style: titleStyle,
              ),
              SizedBox(height: 20),
              ConstrainedBox(
                constraints: BoxConstraints(maxHeight: 200, minHeight: 150),
                child: SingleChildScrollView(
                  child: Text(message, style: messageStyle),
                ),
              ),
            ],
          ),
        ),
        Center(
          child: ViewStateButton(
            child: buttonText,
            textData: buttonTextData,
            onPressed: onPressed,
          ),
        ),
      ],
    );
  }
}

class ViewStateErrorWidget extends StatelessWidget {
  final ViewStateError error;
  final String title;
  final String message;
  final Widget image;
  final Widget buttonText;
  final String buttonTextData;
  final VoidCallback onPressed;

  const ViewStateErrorWidget({
    required Key key,
    required this.error,
    required this.image,
    required this.title,
    required this.message,
    required this.buttonText,
    required this.buttonTextData,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var defaultImage;
    var defaultTitle;
    var errorMessage = error.message;
    String defaultTextData = 'S.of(context).viewStateButtonRetry';
    switch (error.errorType) {
      case ErrorType.networkError:
        defaultImage = Transform.translate(
          offset: Offset(-50, 0),
          child: const Icon(IconFonts.pageNetworkError,
              size: 100, color: Colors.grey),
        );
        defaultTitle = 'S.of(context).viewStateMessageNetworkError';
        errorMessage = '';
        break;
      case ErrorType.defaultError:
        defaultImage =
            const Icon(IconFonts.pageError, size: 100, color: Colors.grey);
        defaultTitle = 'S.of(context).viewStateMessageError';
        break;
    }

    return ViewStateWidget(
      onPressed: this.onPressed,
      image: image,
      title: title,
      message: message,
      buttonTextData: buttonTextData,
      buttonText: buttonText,
      key: Key(title + 'a'),
    );
  }
}

class ViewStateEmptyWidget extends StatelessWidget {
  final String message;
  final Widget image;
  final Widget buttonText;
  final VoidCallback onPressed;

  const ViewStateEmptyWidget(
      {required Key key,
      required this.image,
      required this.message,
      required this.buttonText,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewStateWidget(
      onPressed: this.onPressed,
      image: image,
      title: message,
      buttonText: buttonText,
      buttonTextData: 'Refresh',
      message: '',
      key: Key('12a' + message),
    );
  }
}

class ViewStateButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;
  final String textData;

  const ViewStateButton(
      {required this.onPressed, required this.child, this.textData = ''});
      // : assert(child == null || textData == null);

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      child: child,
      textColor: Colors.white,

      splashColor: Theme.of(context).splashColor,
      onPressed: onPressed,
      highlightedBorderColor: Theme.of(context).splashColor,
    );
  }
}
