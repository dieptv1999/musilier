import 'dart:io';

import 'package:amplify_flutter/amplify.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';
import 'package:path_provider/path_provider.dart';

class AmplifyService {
  static Future<String> getDownloadUrl(key) async {
    try {
      // final GetUrlResult result = await Amplify.Storage.getUrl(
      //     key: 'ExampleKey',
      //     options: GetUrlOptions(accessLevel: StorageAccessLevel.guest));
      // print('Got URL: ${result.url}');
      return 'https://storagebucketname192742-dev.s3.us-west-2.amazonaws.com/public/' +
          key;
    } on StorageException catch (e) {
      print('Error getting download URL: $e');
    }
    return '';
  }

  static Future<void> createAndUploadFile() async {
    // // Create a dummy file
    // final exampleString = 'Example file contents';
    // final tempDir = await getTemporaryDirectory();
    // final exampleFile = File(tempDir.path + '/example.txt')
    //   ..createSync()
    //   ..writeAsStringSync(exampleString);
    //
    // // Upload the file to S3
    // try {
    //   final UploadFileResult result = await Amplify.Storage.uploadFile(
    //     local: exampleFile,
    //     key: 'ExampleKey',
    //   );
    //   print('Successfully uploaded file: ${result.key}');
    // } on StorageException catch (e) {
    //   print('Error uploading file: $e');
    // }
  }
}
