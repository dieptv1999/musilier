import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:musilier/config/net/base_api.dart';
import 'package:musilier/model/account_model.dart';
import 'package:musilier/model/album_model.dart';
import 'package:musilier/model/category_model.dart';
import 'package:musilier/model/song_model.dart';

class BaseRepository {
  static Future fetchSongList(String input, int page, int size) async {
    var response =
        await http.get('/songs?query=${input}&page=${page}&size=${size}');
    return jsonDecode(response.data)
        .map<Song>((item) => Song.fromJsonMap(item))
        .toList();
  }

  static Future fetchAlbum(String input, int page) async {
    var response = await http.get('/album');
    return jsonDecode(response.data)
        .map<Album>((item) => Album.fromJsonMap(item))
        .toList();
  }

  static Future fetchAlbumDetail(int id, int page) async {
    var response = await http.get('/album/${id}');
    return jsonDecode(response.data)
        .map<Song>((item) => Song.fromJsonMap(item))
        .toList();
  }

  static Future fetchCategoryDetail(int id, int page) async {
    var response = await http.get('/category/${id}');
    var _songs = jsonDecode(response.data);
    if (_songs != null) {
      return _songs.map<Song>((item) => Song.fromJsonMap(item)).toList();
    } else
      return null;
  }

  static Future fetchCategoryList({String input = '', int page = 1}) async {
    var response = await http.post('/category', data: {
      'input': input,
      'type': 'netease',
      'page': page,
      'filter': 'name',
    });
    return jsonDecode(response.data)
        .map<Category>((item) => Category.fromJsonMap(item))
        .toList();
  }

  static Future fetchAccount({String input = ''}) async {
    var response = await http.post('/user', data: {
      'input': input,
      'type': 'netease',
      'filter': 'name',
    });
    return Account.fromJsonMap(jsonDecode(response.data)[0]);
  }

  static Future uploadFile(String filepath, String filename) async {
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(filepath, filename: filename),
    });
    var response = await http.post('/upload', data: formData);
    return response.statusCode;
  }

  static Future favorite(int? songId, value) async {
    var response = await http.post(
        '/api/v1/song/favorite?song_id=${songId.toString()}&value=${value.toString()}');
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<List<Song>> fetchFavoriteSongList(String input, int page, int size) async {
    var response = await http
        .get('/api/v1/songs/favorite?query=${input}&page=${page}&size=${size}');
    debugPrint('${response.data}');
    if (jsonDecode(response.data) != null)
      return jsonDecode(response.data).map<Song>((item) => Song.fromJsonMap(item)).toList();
    else
      return [];
  }

  static Future fetchAppReview() async {
    var response = await http.get("/app-review");
    return response.data.toString();
  }
}
