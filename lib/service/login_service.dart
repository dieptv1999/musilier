import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:musilier/config/net/base_api.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:nb_utils/nb_utils.dart';

class LoginService {
  static FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  Future<String> signInWithGoogle() async {
    GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      return googleSignInAuthentication.idToken!;
    } else {
      throw errorSomethingWentWrong;
    }
  }

  Future<void> signOutGoogle() async {
    await googleSignIn.signOut();
  }
}

class LoginRepository {
  static Future forgetPassword(request) async {
    var response = await http.post('/customer/forget-password', data: request);
  }

  static Future socialLoginApi(request) async {
    var response = await http.post('/customer/social_login',
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        }),
        data: jsonEncode({
          'accessToken': request,
        }));
    Map<String, dynamic> result = Map<String, dynamic>.from(response.data);
    Map<String, dynamic> data = jsonDecode(result['info']);
    StorageManager.sharedPreferences!.setBool(IS_SOCIAL_LOGIN, true);
    StorageManager.sharedPreferences!.setString(AVATAR, data['photo_url']);
    StorageManager.sharedPreferences!.setInt(USER_ID, data['id']);
    StorageManager.sharedPreferences!.setString(FIRST_NAME, data['first_name']);
    StorageManager.sharedPreferences!.setString(LAST_NAME, data['last_name']);
    StorageManager.sharedPreferences!.setString(USER_EMAIL, data['user_email']);
    StorageManager.sharedPreferences!.setString(USERNAME, data['username']);
    StorageManager.sharedPreferences!.setString(TOKEN, result['access_token']);
    StorageManager.sharedPreferences!
        .setString(REFRESH_TOKEN, result['refresh_token']);
    StorageManager.sharedPreferences!
        .setString(USER_DISPLAY_NAME, data['user_display_name']);
    StorageManager.sharedPreferences!.setBool(IS_LOGGED_IN, true);
    return response;
  }

  static Future refreshToken() async {
    var response = await http.post('/token/refresh',
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        }),
        data: jsonEncode({
          'refresh_token':
              StorageManager.sharedPreferences!.getString(REFRESH_TOKEN),
        }));
    Map<String, dynamic> result = Map<String, dynamic>.from(response.data);
    StorageManager.sharedPreferences!.setString(TOKEN, result['access_token']);
    return result;
  }

  static Future logout() async {
    await http.delete('/api/v1/logout');
    StorageManager.sharedPreferences!.setBool(IS_SOCIAL_LOGIN, false);
    StorageManager.sharedPreferences!.remove(AVATAR);
    StorageManager.sharedPreferences!.remove(USER_ID);
    StorageManager.sharedPreferences!.remove(FIRST_NAME);
    StorageManager.sharedPreferences!.remove(LAST_NAME);
    StorageManager.sharedPreferences!.remove(USER_EMAIL);
    StorageManager.sharedPreferences!.remove(USERNAME);
    StorageManager.sharedPreferences!.remove(TOKEN);
    StorageManager.sharedPreferences!.remove(REFRESH_TOKEN);
    StorageManager.sharedPreferences!.remove(USER_DISPLAY_NAME);
    StorageManager.sharedPreferences!.setBool(IS_LOGGED_IN, false);
    return true;
  }

  static Future register(request) async {
    try {
      var response = await http.post('/customer/register',
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
          }),
          data: jsonEncode(request));
      return true;
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }

  static Future login(request) async {
    try {
      var response = await http.post('/customer/login',
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
          }),
          data: jsonEncode(request));
      Map<String, dynamic> result = Map<String, dynamic>.from(response.data);
      Map<String, dynamic> data = jsonDecode(result['info']);
      StorageManager.sharedPreferences!.setBool(IS_SOCIAL_LOGIN, true);
      StorageManager.sharedPreferences!.setString(AVATAR, data['photo_url']);
      StorageManager.sharedPreferences!.setInt(USER_ID, data['id']);
      StorageManager.sharedPreferences!.setString(FIRST_NAME, data['first_name']);
      StorageManager.sharedPreferences!.setString(LAST_NAME, data['last_name']);
      StorageManager.sharedPreferences!.setString(USER_EMAIL, data['user_email']);
      StorageManager.sharedPreferences!.setString(USERNAME, data['username']);
      StorageManager.sharedPreferences!.setString(TOKEN, result['access_token']);
      StorageManager.sharedPreferences!
          .setString(REFRESH_TOKEN, result['refresh_token']);
      StorageManager.sharedPreferences!
          .setString(USER_DISPLAY_NAME, data['user_display_name']);
      StorageManager.sharedPreferences!.setBool(IS_LOGGED_IN, true);
      return true;
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }
}
