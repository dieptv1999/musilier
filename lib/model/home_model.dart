import 'dart:math';

import 'package:flutter/material.dart';
import 'package:musilier/model/album_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/view_state_refresh_list_model.dart';
import 'package:musilier/service/base_repository.dart';

class HomeModel extends ViewStateRefreshListModel {
  late List<Album> _albums;
  late List<Song> _forYou;

  HomeModel() : super();
  List<Album> get albums => _albums;

  List<Song> get forYou => _forYou;
  @override
  Future<List<Song>> loadData({int pageNum = 0}) async {
    List<Future> futures = [];
    futures.add(BaseRepository.fetchAlbum('', pageNum));
    futures.add(BaseRepository.fetchSongList('', pageNum, 10));
    var result = await Future.wait(futures);
    _albums = result[0];
    _forYou = result[1];
    return result[1];
  }
}
