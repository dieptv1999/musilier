import 'dart:convert';

import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/provider/view_state_refresh_list_model.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountModel extends ViewStateRefreshListModel {
  Account? _account;

  Account? get account => _account;

  static const kAccount = 'kAccount';

  AccountModel() : super();

  @override
  Future<List<Account>> loadData({int pageNum = 1}) async {
    String _acc = StorageManager.sharedPreferences!.getString(USER_EMAIL) ?? '';
    if (_acc != '') {
      _account = Account(
        first_name: StorageManager.sharedPreferences!.getString(FIRST_NAME) ?? '',
        last_name: StorageManager.sharedPreferences!.getString(LAST_NAME) ?? '',
        photoURL: StorageManager.sharedPreferences!.getString(AVATAR) ?? '',
        user_display_name: StorageManager.sharedPreferences!.getString(USER_DISPLAY_NAME) ?? '',
        user_email: StorageManager.sharedPreferences!.getString(USER_EMAIL) ?? '',
        username: StorageManager.sharedPreferences!.getString(USERNAME) ?? '',
      );
      return [_account!];
    }
    _account = null;
    return [];
  }
}

class Account {
  int? id;
  String photoURL;
  String first_name;
  String last_name;
  String user_email;
  String username;
  String user_display_name;
  int type_login;

  Account({
    required this.photoURL,
    required this.first_name,
    required this.last_name,
    required this.user_email,
    required this.username,
    required this.user_display_name,
    this.type_login = 0,
    this.id
  });

  Account.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        photoURL = map["photoURL"],
        first_name = map["first_name"],
        last_name = map["last_name"],
        user_email = map["user_email"],
        username = map["username"],
        user_display_name = map["user_display_name"],
        type_login = map["type_login"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photoURL'] = photoURL;
    data['first_name'] = first_name;
    data['last_name'] = last_name;
    data['user_email'] = user_email;
    data['username'] = username;
    data['user_display_name'] = user_display_name;
    data['type_login'] = type_login;
    return data;
  }
}
