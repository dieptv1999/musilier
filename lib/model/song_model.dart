import 'dart:async';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:musilier/provider/view_state_refresh_list_model.dart';
import 'package:musilier/service/base_repository.dart';

class FavoriteListSongModel extends ViewStateRefreshListModel<Song> {
  final String input;

  FavoriteListSongModel({this.input = ''}) : super();

  @override
  Future<List<Song>> loadData({int pageNum = 0}) async {
    return await BaseRepository.fetchFavoriteSongList(input, pageNum, 10);
  }
}

class SongListModel extends ViewStateRefreshListModel<Song> {
  final String input;

  SongListModel({this.input = ''}) : super();

  @override
  Future<List<Song>> loadData({int pageNum = 0}) async {
    return await BaseRepository.fetchSongList(input, pageNum, 10);
  }
}

class SongModel with ChangeNotifier {
  late String _url = "";

  String get url => _url;
  int album_id = -1;

  setUrl(String url) {
    _url = url;
    notifyListeners();
  }

  AudioPlayer _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);

  AudioPlayer get audioPlayer => _audioPlayer;

  List<Song> _songs = [];

  bool _isPlaying = false;

  bool get isPlaying => _isPlaying;

  //Phan hen gio
  TimeOfDay _time = TimeOfDay(hour: 0, minute: 0);
  bool isTimer = false;
  late Timer _timer;

  void selectTime(context, Function callback) async {
    if (isTimer == true) {
      isTimer = false;
      _time = TimeOfDay(hour: 0, minute: 0);
      if (_timer != null && _timer.isActive) {
        _timer.cancel();
        callback();
      }
    } else {
      final TimeOfDay? newTime = await showTimePicker(
        helpText: "Cài đặt khoảng thời gian bạn muốn hẹn giờ",
        context: context,
        initialTime: _time,
      );
      if (newTime != null) {
        isTimer = true;
        _time = newTime;
        callback();
      }
    }
    timerSet(isTimer, _time, callback);
  }

  void timerSet(bool isTimer, TimeOfDay timer, callback) async {
    if (isTimer == true) {
      _timer = Timer(Duration(hours: timer.hour, minutes: timer.minute), () {
        isTimer = false;
        _audioPlayer.pause();
        _isPlaying = false;
        callback();
      });
    }
  }

  //Het phan hen gio
  setPlaying(bool isPlaying) {
    _isPlaying = isPlaying;
    notifyListeners();
  }

  bool _isRepeat = true;

  bool get isRepeat => _isRepeat;

  changeRepeat() {
    _isRepeat = !_isRepeat;
    notifyListeners();
  }

  bool _showList = false;

  bool get showList => _showList;

  setShowList(bool showList) {
    _showList = showList;
    notifyListeners();
  }

  int _currentSongIndex = 0;

  List<Song> get songs => _songs;

  setSongs(List<Song> songs) {
    _songs = songs;
    notifyListeners();
  }

  addSongs(List<Song> songs) {
    _songs.addAll(songs);
    notifyListeners();
  }

  int get length => _songs.length;

  int get songNumber => _currentSongIndex + 1;

  setCurrentIndex(int index) {
    _currentSongIndex = index;
    notifyListeners();
  }

  bool _playNow = false;

  bool get playNow => _playNow;

  setPlayNow(bool playNow) {
    _playNow = playNow;
    notifyListeners();
  }

  Song get currentSong => _songs[_currentSongIndex];

  int get currentIndex => _currentSongIndex;

  Song get nextSong {
    if (isRepeat) {
      if (_currentSongIndex < length) {
        _currentSongIndex++;
      }
      if (_currentSongIndex == length) {
        _currentSongIndex = 0;
      }
    } else {
      Random r = new Random();
      _currentSongIndex = r.nextInt(_songs.length);
    }
    notifyListeners();
    return _songs[_currentSongIndex];
  }

  Song get prevSong {
    if (isRepeat) {
      if (_currentSongIndex > 0) {
        _currentSongIndex--;
      }
      if (_currentSongIndex == 0) {
        _currentSongIndex = length - 1;
      }
    } else {
      Random r = new Random();
      _currentSongIndex = r.nextInt(_songs.length);
    }
    notifyListeners();
    return _songs[_currentSongIndex];
  }

  late Duration _position = Duration(seconds: 0);

  Duration get position => _position;

  void setPosition(Duration position) {
    _position = position;
    notifyListeners();
  }

  late Duration _duration = Duration(seconds: 0);

  Duration get duration => _duration;

  void setDuration(Duration duration) {
    _duration = duration;
    notifyListeners();
  }

  void playList(List<Song> data, int id) {
    debugPrint('album_id${id}');
    _songs = data;
    final _random = new Random();
    int next(int min, int max) => min + _random.nextInt(max - min);
    setCurrentIndex(next(0, data.length));
    album_id = id;
    notifyListeners();
  }
}

class Song {
  String type;
  String link;
  int? id;
  String title;
  String author;
  String lrc;
  String url;
  String pic;
  String thumbnail;

  Song({
    required this.title,
    required this.author,
    required this.link,
    required this.lrc,
    required this.pic,
    this.id,
    required this.type,
    required this.url,
    required this.thumbnail,
  });

  Song.fromJsonMap(Map<String, dynamic> map)
      : type = map["type"],
        link = map["link"],
        id = map["id"],
        title = map["title"],
        author = map["author"],
        lrc = map["lrc"],
        url = map["url"],
        pic = map["pic"],
        thumbnail = map['thumbnail'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = type;
    data['link'] = link;
    data['id'] = id;
    data['title'] = title;
    data['author'] = author;
    data['lrc'] = lrc;
    data['url'] = url;
    data['pic'] = pic;
    data['thumbnail'] = thumbnail;
    return data;
  }
}
