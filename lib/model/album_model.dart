
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/view_state_refresh_list_model.dart';
import 'package:musilier/service/base_repository.dart';

class AlbumListModel extends ViewStateRefreshListModel<Song> {
  final int id;

  AlbumListModel({this.id = 0}) : super();

  @override
  Future<List<Song>> loadData({int pageNum = 0}) async {
    return await BaseRepository.fetchAlbumDetail(id, pageNum);
  }
}

class Album {
  int? id;
  String name;
  String pic;
  String description;

  Album({
    required this.name,
    required this.pic,
    this.id,
    required this.description,
  });

  Album.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        name = map["name"],
        description = map["description"],
        pic = map["pic"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['pic'] = pic;
    return data;
  }
}
