import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/view_state_refresh_list_model.dart';
import 'package:musilier/service/base_repository.dart';

class CategoryListModel extends ViewStateRefreshListModel<Song> {
  final int id;

  CategoryListModel({this.id = 0}) : super();

  @override
  Future<List<Song>> loadData({int pageNum = 0}) async {
    return await BaseRepository.fetchCategoryDetail(id, pageNum);
  }
}

class CategoryModel extends ViewStateRefreshListModel {
  late List<Category> _categories = [];

  List<Category> get categories => _categories;

  CategoryModel() : super();

  @override
  Future<List<Category>> loadData({int pageNum = 1}) async {
    List<Future> futures = [];
    futures.add(BaseRepository.fetchCategoryList(page: pageNum));
    var result = await Future.wait(futures);
    _categories = result[0];
    return result[0];
  }
}

class Category {
  int? id;
  int type;
  String link;
  String name;
  String pic;

  Category({
    required this.link,
    required this.pic,
    this.id,
    required this.type,
    required this.name,
  });

  Category.fromJsonMap(Map<String, dynamic> map)
      : type = map["type"],
        link = map["link"],
        name = map["name"],
        id = map["id"],
        pic = map["pic"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = type;
    data['link'] = link;
    data['name'] = name;
    data['pic'] = pic;
    data['id'] = id;
    return data;
  }
}
