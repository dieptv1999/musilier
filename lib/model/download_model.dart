import 'dart:io';

import 'package:flutter/material.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/view_state_list_model.dart';
import 'package:http/http.dart';
import 'package:localstorage/localstorage.dart';
import 'package:musilier/service/amplify_service.dart';
import 'package:musilier/service/storage_repository.dart';
import 'package:path_provider/path_provider.dart';

const String kLocalStorageSearch = 'kLocalStorageSearch';
const String kDownloadList = 'kDownloadList';
const String kDirectoryPath = 'kDirectoryPath';

class DownloadListModel extends ViewStateListModel<Song> {
  DownloadModel downloadModel;

  DownloadListModel({required this.downloadModel}) : super();

  @override
  Future<List<Song>> loadData() async {
    LocalStorage localStorage = LocalStorage(kLocalStorageSearch);
    await localStorage.ready;
    List<Song> downloadList =
        (localStorage.getItem(kDownloadList) ?? []).map<Song>((item) {
      return Song.fromJsonMap(item);
    }).toList();
    downloadModel.setDownloads(downloadList);
    setIdle();
    return downloadList;
  }
}

class DownloadModel with ChangeNotifier {
  DownloadModel() {
    _directoryPath =
        StorageManager.sharedPreferences!.getString(kDirectoryPath);
  }

  String? _directoryPath;
  List<Song>? _downloadSong;

  List<Song>? get downloadSong => _downloadSong;

  setDownloads(List<Song> downloadSong) {
    _downloadSong = downloadSong;
    notifyListeners();
  }

  download(Song song) {
    if (_downloadSong!.contains(song)) {
      removeFile(song);
    } else {
      downloadFile(song);
    }
  }

  Future downloadFile(Song s) async {
    final bytes = await readBytes(Uri.parse(await AmplifyService.getDownloadUrl(s.url)));
    debugPrint('${s.url}');
    final dir = await getApplicationDocumentsDirectory();
    setDirectoryPath(dir.path);
    final file = File('${dir.path}/${s.id}.mp3');

    if (await file.exists()) {
      return;
    }

    await file.writeAsBytes(bytes);
    if (await file.exists()) {
      s.url = '${dir.path}/${s.id}.mp3';
      _downloadSong!.add(s);
      saveData();
      notifyListeners();
    }
  }

  String? get getDirectoryPath => _directoryPath;

  setDirectoryPath(String directoryPath) {
    _directoryPath = directoryPath;
    StorageManager.sharedPreferences!
        .setString(kDirectoryPath, _directoryPath!);
  }

  Future removeFile(Song s) async {
    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/${s.id}.mp3');
    setDirectoryPath(dir.path);
    if (await file.exists()) {
      await file.delete();
      _downloadSong!.remove(s);
      saveData();
      notifyListeners();
    }
  }

  saveData() async {
    LocalStorage localStorage = LocalStorage(kLocalStorageSearch);
    await localStorage.ready;
    localStorage.setItem(kDownloadList, _downloadSong);
  }

  isDownload(Song newSong) {
    bool isDownload = false;
    if (_downloadSong != null)
      for (int i = 0; i < _downloadSong!.length; i++) {
        if (_downloadSong![i].id == newSong.id) {
          isDownload = true;
          break;
        }
      }
    return isDownload;
  }
}
