import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:musilier/model/song_model.dart';
import 'package:provider/provider.dart';

class RotatePlayer extends StatefulWidget {
  @override
  RotatePlayerState createState() => RotatePlayerState();
}

class RotatePlayerState extends State<RotatePlayer>
    with TickerProviderStateMixin {
  late AnimationController controllerPlayer;
  late Animation<double> animationPlayer;
  final _commonTween = new Tween<double>(begin: 0.0, end: 1.0);

  Widget build(BuildContext context) {
    SongModel songModel = Provider.of(context);
    if (songModel.isPlaying) {
      controllerPlayer.forward();
    } else {
      controllerPlayer.stop(canceled: false);
    }
    return GestureDetector(
      onTap: () {},
      child: RotationTransition(
        turns: _commonTween.animate(animationPlayer),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.7,
          height: MediaQuery.of(context).size.width * 0.7,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: CachedNetworkImageProvider(songModel.currentSong.pic),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    controllerPlayer = new AnimationController(
        duration: const Duration(milliseconds: 40000), vsync: this);
    animationPlayer =
        new CurvedAnimation(parent: controllerPlayer, curve: Curves.linear);
    animationPlayer.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controllerPlayer.repeat();
      } else if (status == AnimationStatus.dismissed) {
        controllerPlayer.forward();
      }
    });
  }

  @override
  void dispose() {
    controllerPlayer.dispose();
    super.dispose();
  }
}
