import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:musilier/config/resource_manager.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/view/player_page.dart';
import 'package:musilier/view/widgets/UploadWidget.dart';
import 'package:provider/provider.dart';

class RotateRecord extends AnimatedWidget {
  final Animation<double> animation;

  RotateRecord({required Key key, required this.animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    SongModel songModel = Provider.of(context);
    return GestureDetector(
      onTap: () {
        if (songModel.songs != null && songModel.songs.length > 0) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => PlayPage(
                nowPlay: false,
              ),
            ),
          );
        }
      },
      child: Container(
        height: 38.0,
        width: 38.0,
        child: songModel != null &&
                songModel.songs.length > 0 &&
                songModel.currentSong != null
            ? RotationTransition(
                turns: animation,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                          songModel.currentSong.pic,
                          maxHeight: 38,
                          maxWidth: 38),
                    ),
                  ),
                ))
            : PopupMenuButton<String>(
                onSelected: (String result) {
                  showModalBottomSheet(
                      context: context, builder: (_) => UploadWidget() );
                  // upload();
                },
                itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                  const PopupMenuItem(
                    value: "upload",
                    child: Text('Share song for everyone'),
                  ),
                ],
              ),
      ),
    );
  }
}
