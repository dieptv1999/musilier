import 'package:flutter/material.dart';
import 'package:musilier/helper/contant.dart';
import 'package:musilier/model/account_model.dart';
import 'package:musilier/model/favorite_model.dart';
import 'package:musilier/model/local_view_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/service/login_service.dart';
import 'package:musilier/view/login_page.dart';
import 'package:musilier/view/widgets/app_bar.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    FavoriteModel _favor = Provider.of(context);
    return Scaffold(
      body: SafeArea(
          child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              AppBarCarousel(
                title: "Account",
                backBtn: false,
              ),
              Stack(
                children: [
                  ProviderWidget<AccountModel>(
                    key: Key('account_page'),
                    builder: (context, accountModel, child) {
                      if (accountModel.busy) {
                        return ViewStateBusyWidget();
                      } else if (accountModel.error &&
                          accountModel.list.isEmpty) {
                        return ViewStateErrorWidget(
                            error: accountModel.viewStateError!,
                            image: Container(),
                            buttonText: Container(),
                            key: Key('error_search'),
                            message: '',
                            title: '',
                            buttonTextData: '',
                            onPressed: accountModel.initData);
                      }

                      Account? _account = accountModel.account;
                      debugPrint('${_account} _account');
                      return Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                stops: [0.1, 0.5, 0.7, 0.9],
                                colors: [
                                  Colors.purple[800]!,
                                  Colors.purple[700]!,
                                  Colors.purple[600]!,
                                  Colors.purple[500]!,
                                ],
                              ),
                            ),
                            child: _account != null
                                ? Row(
                                    children: [
                                      Container(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12.0, vertical: 8.0),
                                          child: CircleAvatar(
                                            radius: 22.0,
                                            backgroundImage: _account
                                                        .photoURL ==
                                                    ''
                                                ? NetworkImage(
                                                    'https://i.ibb.co/kMvH3sf/51f6fb256629fc755b8870c801092942.png')
                                                : NetworkImage(
                                                    _account.photoURL),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            _account.user_display_name,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.symmetric(
                                                vertical: 2.0),
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 3.0, vertical: 1.0),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.white)),
                                            child: Text('FREE',
                                                style: TextStyle(
                                                  fontSize: 8.5,
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          )
                                        ],
                                      )
                                    ],
                                  )
                                : Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 12.0, vertical: 8.0),
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (_) => LoginPage(),
                                          ),
                                        ).then((value) =>
                                            accountModel.refresh(init: true));
                                      },
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.login_outlined),
                                          ),
                                          Text('Đăng nhập'),
                                        ],
                                      ),
                                    ),
                                  ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {
                                  String _locale =
                                      AppLocalizations.of(context)!.localeName;
                                  LocaleModel _localModel =
                                      Provider.of(context);
                                  return Container(
                                    height: LOCALES.length * 44 + 18,
                                    child: ListView.builder(
                                        itemCount: LOCALES.length,
                                        itemBuilder: (_, index) {
                                          return InkWell(
                                            onTap: () {
                                              _localModel.switchLocale();
                                            },
                                            child: Container(
                                              height: 44,
                                              decoration: BoxDecoration(
                                                  color: _locale ==
                                                          LOCALES[index]
                                                              .languageCode
                                                      ? Colors.white
                                                      : Colors.transparent),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 12.0,
                                                        horizontal: 16.0),
                                                child: Text(
                                                  LOCALES[index]
                                                      .countryCode
                                                      .toString(),
                                                  style: TextStyle(
                                                      color: _locale ==
                                                              LOCALES[index]
                                                                  .languageCode
                                                          ? Colors.black
                                                          : Colors.white),
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  );
                                },
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.language),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Language',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(Icons.chevron_right_rounded)
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () async {
                              launch(await BaseRepository.fetchAppReview());
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.chrome_reader_mode),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'App reviews',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(Icons.chevron_right_rounded)
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              launch(
                                  'https://maplemedia.io/terms-of-service/?fbclid=IwAR09RuiZWRknoTNbr56RLqNKN5Z7D0wQzcl1lc3a4ps56ajMmAziOWjjUZg');
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.content_paste),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Term of Services',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(Icons.chevron_right_rounded)
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              launch('https://www.facebook.com/diep.tv99/');
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.copyright),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Dieptv',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(Icons.chevron_right_rounded)
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              launch(
                                  'mailto:diep.tv1999@gmail.com?subject=[Musilier] Question');
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.mail),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Contact us',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(Icons.chevron_right_rounded)
                                ],
                              ),
                            ),
                          ),
                          _account != null
                              ? InkWell(
                                  onTap: () async {
                                    setState(() => isLoading = true);
                                    bool isLogout =
                                        await LoginRepository.logout();
                                    if (isLogout) {
                                      _favor.refresh();
                                      accountModel.refresh(init: true);
                                    }
                                    setState(() => isLoading = false);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Icon(Icons.logout_rounded),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: Text(
                                                'Logout',
                                                style: TextStyle(
                                                  fontSize: 15,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Icon(Icons.chevron_right_rounded)
                                      ],
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      );
                    },
                    model: AccountModel(),
                    child: Container(),
                    onModelReady: (accountModel) async {
                      await accountModel.initData();
                    },
                  ),
                  isLoading
                      ? Container(
                          child: CircularProgressIndicator(),
                          alignment: Alignment.center,
                        )
                      : SizedBox(),
                ],
              ),
            ],
          ),
        ),
      )),
    );
  }
}
