import 'package:flutter/material.dart';
import 'package:musilier/model/home_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/view/widgets/albums_carousel.dart';
import 'package:musilier/anims/record_anim.dart';
import 'package:musilier/view/widgets/for_you_carousel.dart';
import 'package:musilier/view/search_page.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  late AnimationController controllerRecord;
  late Animation<double> animationRecord;
  final _inputController = TextEditingController();
  final _commonTween = new Tween<double>(begin: 0.0, end: 1.0);

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    controllerRecord = new AnimationController(
        duration: const Duration(milliseconds: 15000), vsync: this);
    animationRecord =
        new CurvedAnimation(parent: controllerRecord, curve: Curves.linear);
    animationRecord.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controllerRecord.repeat();
      } else if (status == AnimationStatus.dismissed) {
        controllerRecord.forward();
      }
    });
  }

  @override
  void dispose() {
    _inputController.dispose();
    controllerRecord.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SongModel songModel = Provider.of(context);
    if (songModel.isPlaying) {
      controllerRecord.forward();
    } else {
      controllerRecord.stop(canceled: false);
    }
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          decoration: BoxDecoration(
            image: new DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("assets/images/background.jpg")),
          ),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
                colors: <Color>[
                  Color(0xFF373331).withOpacity(0.4),
                  Color(0xFF373331).withOpacity(0.65),
                  Color(0xFF373331).withOpacity(0.75),
                  Color(0xFF373331).withOpacity(0.9),
                  Color(0xFF373331),
                ],
                stops: [0.0, 0.1, 0.3, 0.6, 1.0],
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 28.0),
              child: ProviderWidget<HomeModel>(
                  onModelReady: (homeModel) async {
                    await homeModel.initData();
                  },
                  key: Key('home_page'),
                  child: Container(),
                  model: HomeModel(),
                  autoDispose: false,
                  builder: (context, homeModel, child) {
                    if (homeModel.busy) {
                      return ViewStateBusyWidget(
                        size: 'large',
                      );
                    } else if (homeModel.error && homeModel.list.isEmpty) {
                      return ViewStateErrorWidget(
                          error: homeModel.viewStateError!,
                          image: Container(),
                          buttonText: Text('Reload'),
                          key: Key('error_search'),
                          message: '',
                          title: '',
                          buttonTextData: '',
                          onPressed: homeModel.initData);
                    }
                    var albums = homeModel.albums;
                    var forYou = homeModel.forYou;
                    return Column(children: <Widget>[
                      Container(
                        height: 80,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                                    height: 38,
                                    decoration: BoxDecoration(
                                      color:
                                          Theme.of(context).accentColor.withAlpha(50),
                                      borderRadius: BorderRadius.circular(38.0),
                                    ),
                                    child: TextField(
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.grey,
                                      ),
                                      maxLines: 1,
                                      controller: _inputController,
                                      onChanged: (value) {},
                                      onSubmitted: (value) {
                                        if (value.isNotEmpty == true) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) => SearchPage(
                                                input: value,
                                              ),
                                            ),
                                          );
                                        }
                                      },
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        prefixIcon: Icon(
                                          Icons.search,
                                          color: Colors.grey,
                                          size: 18,
                                        ),
                                        isDense: true,
                                        contentPadding: EdgeInsets.all(8),
                                        hintText: AppLocalizations.of(context)!
                                            .searchSuggest,
                                      ),
                                    )),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 20.0),
                                child: RotateRecord(
                                  animation: _commonTween.animate(controllerRecord),
                                  key: Key('rotate_home'),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: SmartRefresher(
                          controller: homeModel.refreshController,
                          onRefresh: () async {
                            await homeModel.refresh();
                            homeModel.showErrorMessage(context);
                          },
                          child: ListView(children: <Widget>[
                            AlbumsCarousel(albums),
                            ForYouCarousel(forYou),
                          ]),
                        ),
                      )
                    ]);
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
