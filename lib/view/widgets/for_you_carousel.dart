import 'package:flutter/material.dart';
import 'package:musilier/model/home_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/view/player_page.dart';
import 'package:musilier/view/widgets/song_item.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ForYouCarousel extends StatefulWidget {
  final List<Song> forYou;

  ForYouCarousel(this.forYou);

  @override
  _ForYouCarouselState createState() => _ForYouCarouselState();
}

class _ForYouCarouselState extends State<ForYouCarousel> {
  static const _pageSize = 10;
  int page = 0;
  bool isInit = false;
  bool loading = false;

  late ScrollController _pagingController;

  void _scrollListener(BuildContext context) async {
    print(_pagingController.position.extentAfter);
    if (_pagingController.position.extentAfter < 200) {
      setState(() => loading = true);
      HomeModel _model = Provider.of(context);
      await _model.loadMore();
      setState(() => loading = false);
    }
  }

  @override
  void initState() {
    _pagingController = new ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!isInit) {
      _pagingController = new ScrollController()
        ..addListener(() {
          _scrollListener(context);
        });
      setState(() => isInit = true);
    }
    return Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 4.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(AppLocalizations.of(context)!.forYou,
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2)),
            GestureDetector(
              onTap: () => {
                print('View All'),
              },
              child: Container(),
              // child: Text(AppLocalizations.of(context)!.viewAll,
              //     style: TextStyle(
              //       color: Colors.grey,
              //       fontSize: 12.0,
              //       fontWeight: FontWeight.w600,
              //     )),
            ),
          ],
        ),
      ),
      ListView.builder(
        shrinkWrap: true,
        controller: _pagingController,
        // physics: new NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: widget.forYou.length,
        itemBuilder: (BuildContext context, int index) {
          Song data = widget.forYou[index];
          return GestureDetector(
            onTap: () {
              if (null != data.url) {
                SongModel songModel = Provider.of(context, listen: false);
                songModel.setSongs(new List<Song>.from(widget.forYou));
                songModel.setCurrentIndex(index);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => PlayPage(
                      nowPlay: true,
                    ),
                  ),
                );
              }
            },
            child: SongItem(data: data),
          );
        },
      ),
      loading
          ? ViewStateBusyWidget(
              size: 'large',
            )
          : Container(),
    ]);
  }
}
