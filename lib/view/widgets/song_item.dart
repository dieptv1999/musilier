import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/model/favorite_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:musilier/view/login_page.dart';
import 'package:provider/provider.dart';

class SongItem extends StatefulWidget {
  final Song data;

  SongItem({required this.data}) : super();

  @override
  State<StatefulWidget> createState() => SongItemState();
}

class SongItemState extends State<SongItem> {
  bool loadingFavorite = false;

  void actionFavorite(favoriteModel, Song data, context) async {
    setState(() => loadingFavorite = true);
    final token = StorageManager.sharedPreferences!.getString(TOKEN);
    if (token != null && token != '') {
      bool result = await BaseRepository.favorite(
          data.id, !favoriteModel.isCollect(data));
      debugPrint("${result}result");

      if (result)
        favoriteModel.collect(data);
      else {
        final snackBar = SnackBar(
            duration: const Duration(milliseconds: 2000),
            width: 280.0,
            // Width of the SnackBar.
            padding: const EdgeInsets.symmetric(
              horizontal: 8.0, // Inner padding for SnackBar content.
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            content: Text('Action fail!'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } else {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Bạn cần đăng nhập để tiếp tục'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => LoginPage(),
                ),
              ).then((value) => Navigator.pop(context, 'Ok')),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
    setState(() => loadingFavorite = false);
  }

  @override
  Widget build(BuildContext context) {
    FavoriteModel favoriteModel = Provider.of(context);
    SongModel songModel = Provider.of(context);
    bool song_playing = songModel.songs.length > 0
        ? widget.data.id == songModel.currentSong.id
        : false;
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0, left: 8.0, right: 8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12.0),
        child: Container(
          padding: EdgeInsets.all(8.0),
          decoration: song_playing
              ? BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.1, 0.5, 0.7, 0.9],
                    colors: [
                      Colors.purple[800]!,
                      Colors.purple[700]!,
                      Colors.purple[600]!,
                      Colors.purple[500]!,
                    ],
                  ),
                )
              : BoxDecoration(),
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(12.0),
                child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Image(
                      image: CachedNetworkImageProvider(widget.data.thumbnail),
                      fit: BoxFit.cover,
                    )),
              ),
              SizedBox(
                width: 18.0,
              ),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.data.title,
                        style: widget.data.url == null
                            ? TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.w600,
                                color: Colors.grey,
                              )
                            : TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.w600,
                              ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        widget.data.author,
                        style: widget.data.url == null
                            ? TextStyle(
                                fontSize: 10.0,
                                color: Colors.grey,
                              )
                            : TextStyle(
                                fontSize: 10.0,
                                color: Colors.grey,
                              ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ]),
              ),
              Visibility(
                visible: song_playing,
                child: Container(
                  height: 30,
                  width: 30,
                  child: Lottie.asset('assets/lottie/playing.json'),
                ),
              ),
              IconButton(
                onPressed: () =>
                    actionFavorite(favoriteModel, widget.data, context),
                icon: loadingFavorite
                    ? SizedBox(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                        ))
                    : widget.data.url == null
                        ? Icon(
                            Icons.favorite_border,
                            color: Colors.grey,
                            size: 20.0,
                          )
                        : favoriteModel.isCollect(widget.data)
                            ? Icon(
                                Icons.favorite,
                                color: Theme.of(context).accentColor,
                                size: 20.0,
                              )
                            : Icon(
                                Icons.favorite_border,
                                size: 20.0,
                              ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
