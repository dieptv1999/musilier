import 'package:flutter/material.dart';
import 'package:musilier/model/album_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/view/player_page.dart';
import 'package:musilier/view/widgets/song_item.dart';
import 'package:provider/provider.dart';

class AlbumCarousel extends StatefulWidget {
  List<Song> songs;

  AlbumCarousel({required this.songs});

  @override
  _AlbumCarouselState createState() => _AlbumCarouselState();
}

class _AlbumCarouselState extends State<AlbumCarousel> {

  @override
  Widget build(BuildContext context) {
    return Container(
            child: ListView.builder(
              shrinkWrap: true,
              physics: new NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: widget.songs.length,
              itemBuilder: (BuildContext context, int index) {
                Song data = widget.songs[index];
                return GestureDetector(
                  onTap: () {
                    if (null != data.url) {
                      SongModel songModel = Provider.of(context, listen: false);
                      songModel.setSongs(widget.songs);
                      songModel.setCurrentIndex(index);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => PlayPage(
                            nowPlay: true,
                          ),
                        ),
                      );
                    }
                  },
                  child: SongItem(data: data),
                );
              },
            ),
    );
  }
}
