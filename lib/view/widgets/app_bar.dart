import 'package:flutter/material.dart';

class AppBarCarousel extends StatelessWidget {
  late String title = '';
  final bool backBtn;

  AppBarCarousel({this.title = '', this.backBtn = true});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          backBtn
              ? IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    size: 25.0,
                    color: Colors.grey,
                  ),
                  onPressed: () => {
                    Navigator.pop(context),
                  },
                )
              : IconButton(
                  icon: Container(),
                  onPressed: () {},
                ),
          Text(
            title,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
          IconButton(
            icon: Icon(
              Icons.more_vert,
              size: 25.0,
              color: Colors.grey,
            ),
            onPressed: () => {},
          ),
        ],
      ),
    );
  }
}
