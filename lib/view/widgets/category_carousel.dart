import 'package:flutter/material.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/model/album_model.dart';
import 'package:musilier/model/category_model.dart';
import 'package:musilier/model/favorite_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:musilier/view/login_page.dart';
import 'package:musilier/view/player_page.dart';
import 'package:musilier/view/widgets/song_item.dart';
import 'package:provider/provider.dart';

class CategoryCarousel extends StatefulWidget {
  int? id;

  CategoryCarousel({required this.id});

  @override
  _CategoryCarouselState createState() => _CategoryCarouselState();
}

class _CategoryCarouselState extends State<CategoryCarousel> {
  void actionFavorite(favoriteModel, Song data, context) async {
    final token = StorageManager.sharedPreferences!.getString(TOKEN);
    if (favoriteModel.isCollect(data) || (token != null && token != '')) {
      bool result = await BaseRepository.favorite(
          data.id, !favoriteModel.isCollect(data));
      debugPrint("${result}result");

      if (result)
        favoriteModel.collect(data);
      else {
        final snackBar = SnackBar(
            duration: const Duration(milliseconds: 2000),
            width: 280.0,
            // Width of the SnackBar.
            padding: const EdgeInsets.symmetric(
              horizontal: 8.0, // Inner padding for SnackBar content.
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            content: Text('Action fail!'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } else {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Bạn cần đăng nhập để tiếp tục'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => LoginPage(),
                ),
              ).then((value) => Navigator.pop(context, 'Ok')),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<CategoryListModel>(
        key: Key('Category_carousel'),
        onModelReady: (model) async {
          await model.initData();
        },
        child: Container(),
        model: CategoryListModel(id: widget.id!),
        builder: (context, model, child) {
          if (model.busy) {
            return ViewStateBusyWidget(
              size: 'large',
            );
          } else if (model.error && model.list.isEmpty) {
            return ViewStateErrorWidget(
                error: model.viewStateError!,
                image: Container(),
                buttonText: Text("Reload"),
                key: Key('error_search'),
                message: '',
                title: '',
                buttonTextData: '',
                onPressed: model.initData);
          }
          if (model.list != null)
            return Container(
              child: ListView.builder(
                shrinkWrap: true,
                physics: new NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: model.list.length,
                itemBuilder: (BuildContext context, int index) {
                  Song data = model.list[index];
                  return GestureDetector(
                    onTap: () {
                      if (null != data.url) {
                        SongModel songModel =
                            Provider.of(context, listen: false);
                        songModel.setSongs(model.list);
                        songModel.setCurrentIndex(index);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => PlayPage(
                              nowPlay: true,
                            ),
                          ),
                        );
                      }
                    },
                    child: SongItem(data: data),
                  );
                },
              ),
            );
          return Center(
            child: Text('No Song'),
          );
        });
  }
}
