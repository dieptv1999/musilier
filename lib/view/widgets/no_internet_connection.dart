import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:musilier/utils/app_widget.dart';

class NoInternetConnection extends StatelessWidget {
  bool _isCloseApp = false;

  NoInternetConnection({isCloseApp = false}) {
    _isCloseApp = isCloseApp;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Lottie.asset("assets/lottie/404.json",
            height: 250,
          ),
          // 30.height,
          // Text(keyString(context, 'lbl_no_internet')!, style: boldTextStyle(size: 24)),
          // 10.height,
          // Text(
          //   keyString(context, 'lbl_network_msg')!,
          //   // style: secondaryTextStyle(
          //   //   size: 14,
          //   // ),
          //   textAlign: TextAlign.center,
          // ).paddingOnly(left: 20, right: 20),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: AppBtn(
              value: "Close & Try again",
              onPressed: () {
                if (_isCloseApp) {
                  SystemNavigator.pop();
                } else {
                  Navigator.of(context).pop();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
