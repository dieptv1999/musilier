import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musilier/service/base_repository.dart';

class UploadWidget extends StatefulWidget {
  @override
  _UploadState createState() => _UploadState();
}

class _UploadState extends State<UploadWidget> {
  final _formKey = GlobalKey<FormState>();
  List<String> categories = ["ac", "vi"];
  String _category = "";
  String _songName = "";
  String _filepath = "";

  @override
  void initState() {
    _category = categories[0];
  }

  void upload() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.audio,
    );

    if (result != null) {
      // File file = File(result.files.single.path.toString());
      // BaseRepository.uploadFile(file);
      setState(() => _filepath = result.files.single.path.toString());
    } else {
      // User canceled the picker
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: [
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter song name';
                }
                return null;
              },
              onChanged: (newValue) {
                setState(() => _songName = newValue.toString());
              },
              decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                filled: true,
                hintText: "Song name",
              ),
            ),
            SizedBox(
              height: 10,
            ),
            DropdownButtonFormField(
              items: categories.map((category) {
                return new DropdownMenuItem(
                    value: category,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.star),
                        Text(category),
                      ],
                    ));
              }).toList(),
              onChanged: (newValue) {
                setState(() => _category = newValue.toString());
              },
              value: _category,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                filled: true,
                hintText: "Category",
              ),
            ),
            SizedBox(
              height: 10,
            ),
            _filepath != ""
                ? TextFormField(
                    enabled: false,
                    initialValue: _filepath,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                      filled: true,
                      hintText: "Song path",
                    ),
                  )
                : ElevatedButton(
                    onPressed: () {
                      upload();
                    },
                    child: const Text('Upload song'),
                  ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState != null &&
                      _formKey.currentState!.validate()) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Processing Data')),
                    );
                  } else {
                    BaseRepository.uploadFile(_filepath, _songName);
                  }
                },
                child: const Text('Submit'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
