import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:musilier/model/album_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:musilier/view/album_detail.dart';

class AlbumsCarousel extends StatefulWidget {
  final List<Album> albums;

  AlbumsCarousel(this.albums);

  @override
  _AlbumsCarouselState createState() => _AlbumsCarouselState();
}

class _AlbumsCarouselState extends State<AlbumsCarousel> {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(AppLocalizations.of(context)!.popular,
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2)),
            GestureDetector(
              onTap: () => {
                print('View All'),
              },
              child: Container(),
              // child: Text(AppLocalizations.of(context)!.viewAll,
              //     style: TextStyle(
              //       color: Colors.grey,
              //       fontSize: 12.0,
              //       fontWeight: FontWeight.w600,
              //     )),
            ),
          ],
        ),
      ),
      Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.width * 0.7,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: widget.albums.length,
              itemBuilder: (BuildContext context, int index) {
                Album data = widget.albums[index];
                return GestureDetector(
                  onTap: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => AlbumDetail(
                          data: data,
                        ),
                      ),
                    ),
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: index == widget.albums.length - 1
                        ? EdgeInsets.only(right: 20.0)
                        : EdgeInsets.only(right: 0.0),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Image(
                                height: MediaQuery.of(context).size.width * 0.7 - 44,
                                width: MediaQuery.of(context).size.width * 0.7 - 44,
                                image: CachedNetworkImageProvider(data.pic),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            data.name,
                            style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.w600,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            data.description,
                            style: TextStyle(
                              fontSize: 10.0,
                              color: Colors.grey,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      )
    ]);
  }
}
