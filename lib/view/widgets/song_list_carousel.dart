import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:musilier/model/favorite_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/view/widgets/app_bar.dart';
import 'package:provider/provider.dart';

class SongListCarousel extends StatefulWidget {
  @override
  _ForYouCarouselState createState() => _ForYouCarouselState();
}

class _ForYouCarouselState extends State<SongListCarousel> {
  Widget _buildSongItem(Song data, index) {
    FavoriteModel favoriteModel = Provider.of(context);
    SongModel songModel = Provider.of(context, listen: true);
    return InkWell(
      onTap: () {
        songModel.setCurrentIndex(index);
      },
      child: data.id == songModel.currentSong.id
          ? Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12.0),
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [0.1, 0.5, 0.7, 0.9],
                      colors: [
                        Colors.purple[800]!,
                        Colors.purple[700]!,
                        Colors.purple[600]!,
                        Colors.purple[500]!,
                      ],
                    ),
                  ),
                  child: Row(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Container(
                            width: 50,
                            height: 50,
                            child: Image.network(
                              data.pic,
                              fit: BoxFit.cover,
                            )),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      Expanded(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                data.title,
                                style: data.url == null
                                    ? TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.grey,
                                      )
                                    : TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                data.author,
                                style: data.url == null
                                    ? TextStyle(
                                        fontSize: 10.0,
                                        color: Colors.grey,
                                      )
                                    : TextStyle(
                                        fontSize: 10.0,
                                      ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ]),
                      ),
                      Container(
                        height: 30,
                        width: 30,
                        child: Lottie.asset('assets/lottie/playing.json'),
                      ),
                    ],
                  ),
                ),
              ),
            )
          : Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
              child: Row(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: Container(
                        width: 50,
                        height: 50,
                        child: Image.network(
                          data.thumbnail,
                          fit: BoxFit.cover,
                        )),
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                  Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            data.title,
                            style: data.url == null
                                ? TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey,
                                  )
                                : TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w600,
                                  ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            data.author,
                            style: data.url == null
                                ? TextStyle(
                                    fontSize: 10.0,
                                    color: Colors.grey,
                                  )
                                : TextStyle(
                                    fontSize: 10.0,
                                  ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ]),
                  ),
                  IconButton(
                    onPressed: () => favoriteModel.collect(data),
                    icon: Icon(Icons.play_arrow),
                  ),
                ],
              ),
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SongModel songModel = Provider.of(context);
    return Expanded(
      child: Column(
        children: [
          SizedBox(
            height: 28,
          ),
          AppBarCarousel(
            title: 'List play music',
            backBtn: false,
          ),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: songModel.songs.length,
              itemBuilder: (BuildContext context, int index) {
                Song data = songModel.songs[index];
                return GestureDetector(
                  onTap: () {
                    if (null != data.url) {
                      songModel.setCurrentIndex(index);
                      songModel.setPlayNow(true);
                      Future.delayed(new Duration(milliseconds: 100), () {
                        songModel.setPlayNow(false);
                      });
                    }
                  },
                  child: _buildSongItem(data, index),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
