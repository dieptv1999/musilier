import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:musilier/model/album_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/view/player_page.dart';
import 'package:musilier/view/widgets/album_carousel.dart';
import 'package:musilier/view/widgets/app_bar.dart';
import 'package:musilier/model/song_model.dart';
import 'package:provider/provider.dart';

class AlbumDetail extends StatefulWidget {
  Album? data;

  AlbumDetail({this.data});

  @override
  _AlbumDetailState createState() => _AlbumDetailState();
}

class _AlbumDetailState extends State<AlbumDetail> {
  @override
  Widget build(BuildContext context) {
    SongModel songModel = Provider.of(context, listen: true);
    return ProviderWidget<AlbumListModel>(
      key: Key('album_carousel'),
      onModelReady: (model) async {
        await model.initData();
      },
      child: Container(),
      model: AlbumListModel(id: widget.data!.id!),
      builder: (context, model, child) {
        debugPrint('${widget.data} ${songModel.album_id} annn,,');
        if (model.busy) {
          return ViewStateBusyWidget(
            size: 'large',
          );
        } else if (model.error && model.list.isEmpty) {
          return ViewStateErrorWidget(
              error: model.viewStateError!,
              image: Container(),
              buttonText: Text('Reload'),
              key: Key('error_search'),
              message: '',
              title: '',
              buttonTextData: '',
              onPressed: model.initData);
        }
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(widget.data!.pic)),
            ),
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                    Color(0xFF373331).withOpacity(0.5),
                    Color(0xFF373331).withOpacity(0.75),
                    Color(0xFF373331).withOpacity(0.85),
                    Color(0xFF373331).withOpacity(0.9),
                    Color(0xFF373331),
                  ],
                  stops: [0.0, 0.1, 0.3, 0.6, 1.0],
                ),
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 28,
                  ),
                  AppBarCarousel(
                    title: widget.data!.name,
                  ),
                  Expanded(
                    child: ListView(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            height: MediaQuery.of(context).size.width * 0.5,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(15.0),
                                child: Container(
                                    decoration:
                                        BoxDecoration(color: Colors.white),
                                    child: Image.network(
                                      widget.data!.pic,
                                      fit: BoxFit.cover,
                                    ))),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        Center(
                          child: Text(
                            widget.data!.name,
                            style: TextStyle(fontSize: 12.0),
                          ),
                        ),
                        AlbumCarousel(songs: model.list),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: Padding(
            padding: const EdgeInsets.only(bottom: 30.0),
            child: songModel.album_id != -1 &&
                    songModel.album_id == widget.data!.id! && songModel.isPlaying
                ? FloatingActionButton(
                    onPressed: () async {
                      final result = await songModel.audioPlayer.pause();
                      songModel.setPlaying(false);
                    },
                    child: Icon(Icons.pause),
                  )
                : FloatingActionButton(
                    onPressed: () {
                      if (songModel.album_id != widget.data!.id!) {
                        songModel.playList(model.list, widget.data!.id!);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) =>
                                PlayPage(
                                  nowPlay: true,
                                ),
                          ),
                        );
                      }else {
                        songModel.audioPlayer.resume();
                        songModel.setPlaying(true);
                      }
                    },
                    child: const Icon(Icons.play_arrow),
                    backgroundColor: Colors.green,
                  ),
          ),
        );
      },
    );
  }
}
