import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:musilier/anims/player_anim.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/model/download_model.dart';
import 'package:musilier/model/favorite_model.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:musilier/view/login_page.dart';
import 'package:musilier/view/widgets/app_bar.dart';
import 'package:musilier/view/widgets/player_carousel.dart';
import 'package:musilier/view/widgets/song_list_carousel.dart';
import 'package:provider/provider.dart';

class PlayPage extends StatefulWidget {
  final bool nowPlay;

  PlayPage({required this.nowPlay});

  @override
  _PlayPageState createState() => _PlayPageState();
}

class _PlayPageState extends State<PlayPage> with TickerProviderStateMixin {
  bool isTimer = false;

  @override
  initState() {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    super.initState();
  }

  void actionFavorite(favoriteModel, Song data, context) async {
    final token = StorageManager.sharedPreferences!.getString(TOKEN);
    if (favoriteModel.isCollect(data) || (token != null && token != '')) {
      bool result = await BaseRepository.favorite(
          data.id, !favoriteModel.isCollect(data));
      debugPrint("${result}result");

      if (result)
        favoriteModel.collect(data);
      else {
        final snackBar = SnackBar(
            duration: const Duration(milliseconds: 2000),
            width: 280.0,
            // Width of the SnackBar.
            padding: const EdgeInsets.symmetric(
              horizontal: 8.0, // Inner padding for SnackBar content.
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            content: Text('Action fail!'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } else {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Bạn cần đăng nhập để tiếp tục'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => LoginPage(),
                ),
              ).then((value) => Navigator.pop(context, 'Ok')),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SongModel songModel = Provider.of(context, listen: true);
    DownloadModel downloadModel = Provider.of(context);
    FavoriteModel favouriteModel = Provider.of(context);
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    image:
                        CachedNetworkImageProvider(songModel.currentSong.pic)),
              ),
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                      Color(0xFF373331).withOpacity(0.3),
                      Color(0xFF373331).withOpacity(0.6),
                      Color(0xFF373331).withOpacity(0.7),
                      Color(0xFF373331),
                      Color(0xFF373331),
                    ],
                    stops: [0.0, 0.2, 0.4, 0.8, 1.0],
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    !songModel.showList
                        ? Column(
                            children: <Widget>[
                              SizedBox(
                                height: 34,
                              ),
                              AppBarCarousel(),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.05),
                              RotatePlayer(),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.03),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.02),
                              Text(
                                songModel.currentSong.author,
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 15.0),
                              ),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.01),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  songModel.currentSong.title,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                              Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    IconButton(
                                      onPressed: () => songModel
                                          .setShowList(!songModel.showList),
                                      icon: Icon(
                                        Icons.list,
                                        size: 25.0,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    IconButton(
                                      onPressed: () => songModel.changeRepeat(),
                                      icon: songModel.isRepeat == true
                                          ? Icon(
                                              Icons.repeat,
                                              size: 25.0,
                                              color: Colors.grey,
                                            )
                                          : Icon(
                                              Icons.shuffle,
                                              size: 25.0,
                                              color: Colors.grey,
                                            ),
                                    ),
                                    IconButton(
                                      onPressed: () => actionFavorite(
                                          favouriteModel,
                                          songModel.currentSong,
                                          context),
                                      icon: favouriteModel.isCollect(
                                                  songModel.currentSong) ==
                                              true
                                          ? Icon(
                                              Icons.favorite,
                                              size: 25.0,
                                              color:
                                                  Theme.of(context).accentColor,
                                            )
                                          : Icon(
                                              Icons.favorite_border,
                                              size: 25.0,
                                              color: Colors.grey,
                                            ),
                                    ),
                                    IconButton(
                                      onPressed: () =>
                                          songModel.selectTime(context, () {
                                        setState(() => isTimer = !isTimer);
                                      }),
                                      icon: isTimer == true
                                          ? Icon(
                                              Icons.timer_off,
                                              size: 25.0,
                                              color:
                                                  Theme.of(context).accentColor,
                                            )
                                          : Icon(
                                              Icons.timer,
                                              size: 25.0,
                                              color: Colors.grey,
                                            ),
                                    ),
                                    // IconButton(
                                    //   onPressed: () => downloadModel
                                    //       .download(songModel.currentSong),
                                    //   icon: downloadModel
                                    //           .isDownload(songModel.currentSong)
                                    //       ? Icon(
                                    //           Icons.cloud_done,
                                    //           size: 25.0,
                                    //           color:
                                    //               Theme.of(context).accentColor,
                                    //         )
                                    //       : Icon(
                                    //           Icons.cloud_download,
                                    //           size: 25.0,
                                    //           color: Colors.grey,
                                    //         ),
                                    // ),
                                  ]),
                            ],
                          )
                        : SongListCarousel(),
                  ],
                ),
              ),
            ),
          ),
          Player(
            songData: songModel,
            downloadData: downloadModel,
            nowPlay: widget.nowPlay,
            key: Key('player_main'),
          ),
        ],
      ),
    );
  }
}
