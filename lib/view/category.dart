import 'package:flutter/material.dart';
import 'package:musilier/model/category_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/view/category_detail.dart';
import 'package:musilier/view/widgets/app_bar.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBarCarousel(
              title: "Category",
              backBtn: false,
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 14.0),
              child: ProviderWidget<CategoryModel>(
                key: Key('category_page'),
                autoDispose: true,
                builder: (context, categoryModel, child) {
                  if (categoryModel.busy) {
                    return ViewStateBusyWidget(
                      size: 'large',
                    );
                  } else if (categoryModel.error &&
                      categoryModel.list.isEmpty) {
                    return ViewStateErrorWidget(
                        error: categoryModel.viewStateError!,
                        image: Container(),
                        buttonText: Text('Reload'),
                        key: Key('error_search'),
                        message: '',
                        title: '',
                        buttonTextData: '',
                        onPressed: categoryModel.initData);
                  }

                  debugPrint('category${categoryModel.categories.toString()}');
                  return GridView.builder(
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 200,
                          childAspectRatio: 3 / 4,
                          crossAxisSpacing: 12,
                          mainAxisSpacing: 12),
                      itemCount: categoryModel.categories.length,
                      itemBuilder: (BuildContext ctx, index) {
                        Category cate = categoryModel.categories[index];
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => CategoryDetail(
                                  data: cate,
                                ),
                              ),
                            );
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Stack(
                                    alignment: Alignment.bottomRight,
                                    children: [
                                      Container(
                                        height: double.infinity,
                                        width: double.infinity,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                          child: Image.network(
                                            cate.pic,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 8, right: 8.0),
                                        child: Container(
                                          child: Icon(Icons.play_circle_fill),
                                          decoration: BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                color:
                                                    Colors.grey.withOpacity(0.7),
                                                spreadRadius: 3,
                                                blurRadius: 8,
                                                offset: Offset(0,
                                                    2), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 50,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 6.0),
                                    child: Text(
                                      cate.name,
                                      maxLines: 2,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                },
                model: CategoryModel(),
                child: Container(),
                onModelReady: (categoryModel) async {
                  await categoryModel.initData();
                },
              ),
            )
          ],
        ),
      ),
    ));
  }
}
