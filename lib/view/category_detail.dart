import 'package:flutter/material.dart';
import 'package:musilier/model/category_model.dart';
import 'package:musilier/view/widgets/app_bar.dart';
import 'package:musilier/view/widgets/category_carousel.dart';

class CategoryDetail extends StatefulWidget {
  Category? data;

  CategoryDetail({this.data});

  @override
  _CategoryDetailState createState() => _CategoryDetailState();
}

class _CategoryDetailState extends State<CategoryDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              AppBarCarousel(title: widget.data!.name,),
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          height: MediaQuery.of(context).size.width * 0.5,
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(15.0),
                              child: Container(
                                  child: Image.network(
                                    widget.data!.pic,
                                    fit: BoxFit.cover,
                                  ))),
                        )),
                    SizedBox(height: 20.0),
                    Center(
                      child: Text(
                        widget.data!.name,
                        style: TextStyle(fontSize: 12.0),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        // Expanded(
                        //   flex: 1,
                        //   child: Container(
                        //     height: 70,
                        //     margin: EdgeInsets.only(
                        //         top: 20, bottom: 20, left: 20, right: 10),
                        //     decoration: BoxDecoration(
                        //       border: Border.all(color: Colors.black12, width: 1),
                        //       borderRadius: BorderRadius.circular(20.0),
                        //     ),
                        //     child: Row(
                        //       mainAxisAlignment: MainAxisAlignment.center,
                        //       children: <Widget>[
                        //         Icon(
                        //           Icons.play_arrow,
                        //           color: Theme.of(context).accentColor,
                        //         ),
                        //         SizedBox(
                        //           width: 5,
                        //         ),
                        //         Text(
                        //           'Play',
                        //           style: TextStyle(
                        //               color: Theme.of(context).accentColor),
                        //         ),
                        //       ],
                        //     ),
                        //   ),
                        // ),
                        // Expanded(
                        //   flex: 1,
                        //   child: Container(
                        //     height: 70,
                        //     margin: EdgeInsets.only(
                        //         top: 20, bottom: 20, left: 10, right: 20),
                        //     decoration: BoxDecoration(
                        //       border: Border.all(color: Colors.black12, width: 1),
                        //       borderRadius: BorderRadius.circular(20.0),
                        //     ),
                        //     child: Row(
                        //       mainAxisAlignment: MainAxisAlignment.center,
                        //       children: <Widget>[
                        //         Icon(Icons.add),
                        //         SizedBox(
                        //           width: 5,
                        //         ),
                        //         Text('Add'),
                        //       ],
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                    CategoryCarousel(id: widget.data!.id),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
