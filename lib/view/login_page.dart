import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/service/login_service.dart';
import 'package:musilier/utils/Colors.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:musilier/utils/EmailValidator.dart';
import 'package:musilier/utils/app_widget.dart';
import 'package:musilier/utils/images.dart';
import 'package:musilier/view/account.dart';
import 'package:musilier/view/signup_page.dart';
import 'package:musilier/view/widgets/no_internet_connection.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:the_apple_sign_in/the_apple_sign_in.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var useEmail = TextEditingController();
  var passwordCont = TextEditingController();
  bool isLoading = false;
  bool? isRemember = false;
  final _formKey = GlobalKey<FormState>();
  var autoValidate = false;
  var email = TextEditingController();

  Future loginApiCall(request) async {
    await isNetworkAvailable().then((netk) async {
      if (netk) {
        isLoading = true;
        bool result = await LoginRepository.login(request);
        setState(() {
          isLoading = false;
        });
        if (result){
          Navigator.pop(context);
        }
        else{
          final snackBar = SnackBar(
              duration: const Duration(milliseconds: 2000),
              width: 280.0, // Width of the SnackBar.
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0, // Inner padding for SnackBar content.
              ),
              behavior: SnackBarBehavior.floating,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              content: Text('Login fail or account don\'t exist!'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else {
        setState(() {
          isLoading = false;
        });
        NoInternetConnection().launch(context);
      }
    });
  }

  Future forgotPwdApi(value) async {
    await isNetworkAvailable().then((bool) async {
      setState(() {
        isLoading = true;
      });
      if (bool) {
        var request = {
          'email': value,
        };
        await LoginRepository.forgetPassword(request).then((res) async {
          setState(() {
            isLoading = false;
          });
          toast(res["message"]);
        }).catchError((onError) {
          setState(() {
            isLoading = false;
          });
          log("Error:" + onError.toString());
          // ErrorView(
          //   message: onError.toString(),
          // ).launch(context);
        });
      } else {
        setState(() {
          isLoading = false;
        });
        NoInternetConnection().launch(context);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    if (Platform.isIOS) {
      TheAppleSignIn.onCredentialRevoked!.listen((_) {
        log("Credentials revoked");
      });
    }
    var remember = await getBool(REMEMBER_PASSWORD);
    if (remember) {
      var password = await getString(PASSWORD);
      var email = await getString(EMAIL);
      setState(() {
        useEmail.text = email;
        passwordCont.text = password;
      });
    }
    setState(() {
      isRemember = remember;
    });
  }

  void socialLogin(req, context) async {
    setState(() {
      isLoading = true;
    });
    await LoginRepository.socialLoginApi(req).then((resp) {
      Navigator.pop(context);
    }).catchError((error) {
      toast(error.toString());
      debugPrint(error);
    });
    setState(() {
      isLoading = false;
    });
  }

  void onGoogleSignInTap(context) async {
    var service = LoginService();
    await service.signInWithGoogle().then((res) {
      socialLogin(res, context);
      service.signOutGoogle();
    }).catchError((e) {
      toast(e.toString());
      service.signOutGoogle();
    });
  }

  saveAppleDataWithoutEmail(context) async {
    await getSharedPref().then((pref) {
      log(getStringAsync('appleEmail'));
      log(getStringAsync('appleGivenName'));
      log(getStringAsync('appleFamilyName'));

      var req = {
        'email': getStringAsync('appleEmail'),
        'firstName': getStringAsync('appleGivenName'),
        'lastName': getStringAsync('appleFamilyName'),
        'photoURL': '',
        'accessToken': '12345678',
        'loginType': 'apple',
      };
      socialLogin(req, context);
    });
  }

  saveAppleData(result, context) async {
    StorageManager.sharedPreferences!
        .setString('appleEmail', result.credential.email);
    StorageManager.sharedPreferences!
        .setString('appleGivenName', result.credential.fullName.givenName);
    StorageManager.sharedPreferences!
        .setString('appleFamilyName', result.credential.fullName.familyName);

    log('Email:- ${getStringAsync('appleEmail')}');
    log('appleGivenName:- ${getStringAsync('appleGivenName')}');
    log('appleFamilyName:- ${getStringAsync('appleFamilyName')}');

    var req = {
      'email': result.credential.email,
      'firstName': result.credential.fullName.givenName,
      'lastName': result.credential.fullName.familyName,
      'photoURL': '',
      'accessToken': '12345678',
      'loginType': 'apple',
    };
    socialLogin(req, context);
  }

  void appleLogIn(context) async {
    if (await TheAppleSignIn.isAvailable()) {
      final AuthorizationResult result = await TheAppleSignIn.performRequests([
        AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
      ]);
      switch (result.status) {
        case AuthorizationStatus.authorized:
          log("Result: $result"); //All the required credentials
          if (result.credential!.email == null) {
            saveAppleDataWithoutEmail(context);
          } else {
            saveAppleData(result, context);
          }
          break;
        case AuthorizationStatus.error:
          log("Sign in failed: ${result.error!.localizedDescription}");
          break;
        case AuthorizationStatus.cancelled:
          log('User cancelled');
          break;
      }
    } else {
      toast('Apple SignIn is not available for your device');
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget socialButtons = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          decoration: boxDecorationWithRoundedCorners(
              borderRadius: radius(10) as BorderRadius?,
              backgroundColor: googleColor),
          padding: EdgeInsets.all(8),
          child: Image.asset(ic_google, color: white, width: 24, height: 24),
        ).onTap(() {
          onGoogleSignInTap(context);
        }),
        // 16.width,
        // Container(
        //   decoration: boxDecorationWithRoundedCorners(
        //       borderRadius: radius(10) as BorderRadius?,
        //       backgroundColor: primaryColor),
        //   padding: EdgeInsets.all(8),
        //   child: Image.asset(ic_calling, color: white, width: 24, height: 24),
        // ).onTap(() {
        //   // MobileNumberSignInScreen().launch(context);
        // }),
        // 16.width,
        // Container(
        //   decoration: boxDecorationWithRoundedCorners(
        //       borderRadius: radius(10) as BorderRadius?,
        //       backgroundColor: Colors.black),
        //   padding: EdgeInsets.all(8),
        //   child: Image.asset(ic_apple, color: white, width: 24, height: 24),
        // ).onTap(() {
        //   appleLogIn(context);
        // }).visible(Platform.isIOS),
      ],
    );

    return Scaffold(
      // backgroundColor: appStore.scaffoldBackground,
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/images/splash.png",
                        width: 200, height: 200),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            EditText(
                              hintText: "Email",
                              isPassword: false,
                              mController: useEmail,
                              mKeyboardType: TextInputType.emailAddress,
                              validator: (String? s) {
                                if (s!.trim().isEmpty) {
                                  return "Email" + " " + "Field Required";
                                }
                                if (!EmailValidator.validate(s)) {
                                  return "Please enter a valid email";
                                }
                                return null;
                              },
                            ),
                            20.height,
                            EditText(
                              hintText: "Enter Password*",
                              isPassword: true,
                              mController: passwordCont,
                              isSecure: true,
                              validator: (String? s) {
                                if (s!.trim().isEmpty)
                                  return "Password" + " " + "Field Required";
                                return null;
                              },
                            ),
                            8.height,
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Checkbox(
                              focusColor: primaryColor,
                              activeColor: primaryColor,
                              value: isRemember,
                              onChanged: (bool? value) {
                                setState(() {
                                  isRemember = value;
                                });
                              },
                            ),
                            Text(
                              "Remember Me",
                              style: secondaryTextStyle(
                                size: 16,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          "For got password",
                          style: secondaryTextStyle(
                            size: 16,
                          ),
                        ).onTap(() {
                          forgotPwdDialog(context);
                        }),
                      ],
                    ).paddingOnly(left: 8, right: 16),
                    8.height,
                    AppBtn(
                      value: "Sign In",
                      onPressed: () {
                        hideKeyboard(context);
                        var request = {
                          "user_email": "${useEmail.text}",
                          "password": "${passwordCont.text}"
                        };
                        if (!mounted) return;
                        setState(() {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              isLoading = true;
                            });
                            loginApiCall(request);
                          } else {
                            setState(() {
                              isLoading = false;
                              autoValidate = true;
                            });
                          }
                        });
                      },
                    ).paddingOnly(left: 20, right: 20),
                    20.height,
                    socialButtons,
                    16.height,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Don't have an account?",
                            style: primaryTextStyle(
                              size: 18,
                              color: Colors.white,
                            )),
                        Container(
                          margin: EdgeInsets.only(left: 4),
                          child: GestureDetector(
                              child: Text("Sign Up",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: primaryColor,
                                  )),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => SignUpScreen(),
                                  ),
                                );
                              }),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          isLoading
              ? Container(
                  child: CircularProgressIndicator(),
                  alignment: Alignment.center,
                )
              : SizedBox(),
        ],
      ),
    );
  }

  Future forgotPwdDialog(BuildContext context) async {
    final _formKey1 = GlobalKey<FormState>();
    var autoValidate1 = false;
    var email = TextEditingController();
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(spacing_control),
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: boxDecoration(
              // color: appStore.scaffoldBackground!,
              radius: 10.0,
              // bgColor: appStore.scaffoldBackground,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Forgot password?", style: boldTextStyle(size: 24))
                      .paddingOnly(
                          left: spacing_standard_new.toDouble(),
                          right: spacing_standard_new.toDouble(),
                          top: spacing_standard_new.toDouble()),
                  SizedBox(height: spacing_standard_new.toDouble()),
                  Form(
                    key: _formKey1,
                    // ignore: deprecated_member_use
                    autovalidate: autoValidate1,
                    child: Column(
                      children: [
                        EditText(
                          hintText: "Enter Email*",
                          hintColor: Colors.grey,
                          isPassword: false,
                          mController: email,
                          mKeyboardType: TextInputType.emailAddress,
                          textColor: Colors.black,
                          validator: (String? s) {
                            if (s!.trim().isEmpty)
                              return "Email" + " " + "Field Required";
                            if (!EmailValidator.validate(s)) {
                              return "Please enter a valid email";
                            }
                            return null;
                          },
                        ),
                      ],
                    ).paddingOnly(
                        left: spacing_standard_new.toDouble(),
                        right: spacing_standard_new.toDouble(),
                        bottom: spacing_standard.toDouble()),
                  ),
                  AppBtn(
                    value: "Submit",
                    onPressed: () {
                      if (_formKey1.currentState!.validate()) {
                        hideKeyboard(context);
                        Navigator.of(context).pop();
                        forgotPwdApi(email.text);
                      } else {
                        isLoading = false;
                        autoValidate1 = true;
                      }
                    },
                  ).paddingAll(spacing_standard_new.toDouble()),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
