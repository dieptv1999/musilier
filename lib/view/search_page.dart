import 'package:flutter/material.dart';
import 'package:musilier/view/widgets/app_bar.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/helper/refresh_helper.dart';
import 'package:musilier/view/player_page.dart';
import 'package:musilier/view/widgets/article_skeleton.dart';
import 'package:musilier/view/widgets/skeleton.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchPage extends StatefulWidget {
  final String input;

  SearchPage({this.input = ''});

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  Widget _buildSongItem(Song data) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12.0),
            child: Container(
                width: 50,
                height: 50,
                child: Image.network(
                  data.thumbnail,
                  fit: BoxFit.cover,
                )),
          ),
          SizedBox(
            width: 20.0,
          ),
          Expanded(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    data.title,
                    style: data.url == null
                        ? TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFFE0E0E0),
                          )
                        : TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600,
                          ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    data.author,
                    style: data.url == null
                        ? TextStyle(
                            fontSize: 10.0,
                            color: Color(0xFFE0E0E0),
                          )
                        : TextStyle(
                            fontSize: 10.0,
                            color: Colors.grey,
                          ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ]),
          ),
          data.url == null
              ? Icon(
                  Icons.favorite_border,
                  color: Color(0xFFE0E0E0),
                  size: 20.0,
                )
              : Icon(
                  Icons.favorite_border,
                  size: 20.0,
                )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            AppBarCarousel(title: "Search"),
            Container(
                margin: EdgeInsets.only(bottom: 10),
                alignment: Alignment.center,
                child: Text(AppLocalizations.of(context)!.searchResult +
                    " for " +
                    widget.input)),
            Expanded(
              child: ProviderWidget<SongListModel>(
                  onModelReady: (model) async {
                    await model.initData();
                  },
                  key: Key('search_page'),
                  child: Container(),
                  autoDispose: false,
                  model: SongListModel(input: widget.input),
                  builder: (context, model, child) {
                    if (model.busy) {
                      return SkeletonList(
                        builder: (context, index) => ArticleSkeletonItem(),
                      );
                    } else if (model.error && model.list.isEmpty) {
                      return ViewStateErrorWidget(
                          error: model.viewStateError!,
                          image: Container(),
                          buttonText: Text('Reload'),
                          key: Key('error_search'),
                          message: '',
                          title: '',
                          buttonTextData: '',
                          onPressed: model.initData);
                    } else if (model.empty) {
                      return ViewStateEmptyWidget(
                        onPressed: model.initData,
                        key: Key('empty_search'),
                        buttonText: Text(''),
                        image: Container(),
                        message: '',
                      );
                    }
                    return SmartRefresher(
                      controller: model.refreshController,
                      header: WaterDropHeader(),
                      footer: RefresherFooter(),
                      onRefresh: () async {
                        await model.refresh();
                      },
                      onLoading: () async {
                        await model.loadMore();
                      },
                      enablePullUp: true,
                      child: ListView.builder(
                          itemCount: model.list.length,
                          itemBuilder: (context, index) {
                            Song data = model.list[index];
                            return GestureDetector(
                                onTap: () {
                                  if (null != data.url) {
                                    SongModel songModel = Provider.of(context, listen: false);
                                    songModel.setSongs(model.list);
                                    songModel.setCurrentIndex(index);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) => PlayPage(nowPlay: true),
                                      ),
                                    );
                                  }
                                },
                                child: _buildSongItem(data));
                          }),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
