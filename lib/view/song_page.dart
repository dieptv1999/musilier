import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:musilier/model/song_model.dart';
import 'package:musilier/provider/provider_widget.dart';
import 'package:musilier/provider/view_state_widget.dart';
import 'package:musilier/service/base_repository.dart';
import 'package:musilier/utils/app_widget.dart';
import 'package:musilier/utils/utils.dart';
import 'package:musilier/view/login_page.dart';
import 'package:musilier/view/player_page.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SongPage extends StatefulWidget {
  @override
  _SongPageState createState() => _SongPageState();
}

class _SongPageState extends State<SongPage>
    with AutomaticKeepAliveClientMixin {
  static const _pageSize = 10;

  bool checkLogin = isLoggedIn();

  final PagingController<int, Song> _pagingController =
      PagingController(firstPageKey: 0);

  List<Song> listSong = [];

  @override
  void initState() {
    loadData();
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      List<Song> newItems =
          await BaseRepository.fetchFavoriteSongList('', pageKey, _pageSize);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        int nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
      listSong.addAll(newItems);
    } catch (error) {
      _pagingController.error = error;
    }
  }

  loadData() async {
    final dir = await getApplicationDocumentsDirectory();
    List<FileSystemEntity> files = dir.listSync();
    for (var file in files) {
      print(file.path);
    }
    setState(() {});
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildSongItem(Song data, Function callback) {
    SongModel songModel = Provider.of(context);
    bool song_playing = songModel.songs.length > 0
        ? data.id == songModel.currentSong.id
        : false;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12.0),
        child: Container(
          padding: EdgeInsets.all(8.0),
          decoration: song_playing
              ? BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.1, 0.5, 0.7, 0.9],
                    colors: [
                      Colors.purple[800]!,
                      Colors.purple[700]!,
                      Colors.purple[600]!,
                      Colors.purple[500]!,
                    ],
                  ),
                )
              : BoxDecoration(),
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(12.0),
                child: Container(
                    width: 50,
                    height: 50,
                    child: Image(
                      image: CachedNetworkImageProvider(data.pic),
                      fit: BoxFit.cover,
                    )),
              ),
              SizedBox(
                width: 20.0,
              ),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        data.title,
                        style: data.url == null
                            ? TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.w600,
                                color: Color(0xFFE0E0E0),
                              )
                            : TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.w600,
                              ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        data.author,
                        style: data.url == null
                            ? TextStyle(
                                fontSize: 10.0,
                                color: Color(0xFFE0E0E0),
                              )
                            : TextStyle(
                                fontSize: 10.0,
                                color: Colors.grey,
                              ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ]),
              ),
              Visibility(
                visible: song_playing,
                child: Container(
                  height: 30,
                  width: 30,
                  child: Lottie.asset('assets/lottie/playing.json'),
                ),
              ),
              IconButton(
                onPressed: () async {
                  if (!song_playing) {
                    bool result = await BaseRepository.favorite(data.id, false);
                    if (result)
                      callback();
                    else
                      toast('Can\'t delete the running song');
                  } else
                    toast('Can\'t delete the running song');
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.redAccent,
                  size: 20.0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SongModel songModel = Provider.of(context);
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: new DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("assets/images/background.jpg")),
      ),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
            colors: <Color>[
              Color(0xFF373331).withOpacity(0.5),
              Color(0xFF373331).withOpacity(0.75),
              Color(0xFF373331).withOpacity(0.85),
              Color(0xFF373331).withOpacity(0.9),
              Color(0xFF373331),
            ],
            stops: [0.0, 0.1, 0.3, 0.6, 1.0],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 28.0),
          child: checkLogin
              ? ProviderWidget<FavoriteListSongModel>(
                  key: Key('song_page'),
                  child: Container(),
                  model: FavoriteListSongModel(),
                  onModelReady: (favoriteListModel) async {
                    await favoriteListModel.initData();
                  },
                  autoDispose: false,
                  builder: (context, favoriteListModel, child) {
                    if (favoriteListModel.busy) {
                      return ViewStateBusyWidget(
                        size: 'large',
                      );
                    } else if (favoriteListModel.error &&
                        favoriteListModel.list.isEmpty) {
                      return ViewStateErrorWidget(
                          error: favoriteListModel.viewStateError!,
                          image: Container(),
                          buttonText: Text('Reload'),
                          key: Key('error_search'),
                          message: '',
                          title: '',
                          buttonTextData: '',
                          onPressed: favoriteListModel.initData);
                    }
                    return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!.tabMusic,
                                  style: TextStyle(
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1.2),
                                ),
                                songModel.album_id == -2 && songModel.isPlaying
                                    ? IconButton(
                                        icon: Icon(Icons.pause),
                                        onPressed: () async {
                                          await songModel.audioPlayer.pause();
                                          songModel.setPlaying(false);
                                        },
                                      )
                                    : IconButton(
                                        onPressed: () {
                                          if (songModel.album_id != -2) {
                                            songModel.playList(
                                                favoriteListModel.list, -2);
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (_) => PlayPage(
                                                  nowPlay: true,
                                                ),
                                              ),
                                            );
                                          } else {
                                            songModel.audioPlayer.resume();
                                            songModel.setPlaying(true);
                                          }
                                        },
                                        icon: Icon(
                                          Icons.play_arrow,
                                        ))
                              ],
                            ),
                          ),
                          Expanded(
                            child: SmartRefresher(
                              controller: favoriteListModel.refreshController,
                              onRefresh: () async {
                                favoriteListModel.refresh();
                                favoriteListModel.showErrorMessage(context);
                                _pagingController.refresh();
                              },
                              child: PagedListView<int, Song>(
                                pagingController: _pagingController,
                                builderDelegate:
                                    PagedChildBuilderDelegate<Song>(
                                  itemBuilder: (context, data, index) =>
                                      GestureDetector(
                                    onTap: () {
                                      if (null != data.url) {
                                        SongModel songModel =
                                            Provider.of(context, listen: true);
                                        songModel.setSongs(
                                            new List<Song>.from(listSong));
                                        songModel.setCurrentIndex(index);
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (_) => PlayPage(
                                              nowPlay: true,
                                            ),
                                          ),
                                        );
                                      }
                                    },
                                    child: data != null
                                        ? _buildSongItem(data, () {
                                            favoriteListModel.refreshController
                                                .requestRefresh();
                                          })
                                        : Container(),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ]);
                  },
                )
              : Padding(
                  padding: const EdgeInsets.all(26.0),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Bạn chưa đăng nhập? Đăng nhập để tiếp tục'),
                        SizedBox(
                          height: 20,
                        ),
                        AppBtn(
                            value: "Continue",
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => LoginPage(),
                                ),
                              ).then((value) => setState(
                                  () => checkLogin = isLoggedIn() && true));
                            }),
                      ],
                    ),
                  ),
                ),
        ),
      ),
    ));
  }
}
