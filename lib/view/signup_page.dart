import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:musilier/service/login_service.dart';
import 'package:musilier/utils/Colors.dart';
import 'package:musilier/utils/Constant.dart';
import 'package:musilier/utils/EmailValidator.dart';
import 'package:musilier/utils/app_widget.dart';
import 'package:musilier/view/account.dart';
import 'package:musilier/view/widgets/no_internet_connection.dart';
import 'package:nb_utils/nb_utils.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var usernameCont = TextEditingController();
  var firstname = TextEditingController();
  var lastname = TextEditingController();
  var username = TextEditingController();
  var passwordCont = TextEditingController();
  var confirmPasswordCont = TextEditingController();
  bool isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var autoValidate = false;
  var mUsername;

  Future registerUser(request, context) async {
    await isNetworkAvailable().then((netk) async {
      if (netk) {
        isLoading = true;
        bool result = await LoginRepository.register(request);
        setState(() {
          isLoading = false;
        });
        if (result) {
          final snackBar = SnackBar(
              duration: const Duration(milliseconds: 2000),
              width: 280.0,
              // Width of the SnackBar.
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0, // Inner padding for SnackBar content.
              ),
              behavior: SnackBarBehavior.floating,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              content: Text('Yeah! Register successful!'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          Navigator.pop(context);
        } else {
          final snackBar = SnackBar(
              duration: const Duration(milliseconds: 2000),
              width: 280.0,
              // Width of the SnackBar.
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0, // Inner padding for SnackBar content.
              ),
              behavior: SnackBarBehavior.floating,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              content: Text('Register fail or account existed!'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else {
        setState(() {
          isLoading = false;
        });
        NoInternetConnection().launch(context);
      }
    });
  }

  void signInApi(req) async {
    // setState(() {
    //   isLoading = true;
    // });
    // await getLoginUserRestApi(req).then((res) async {
    //   if (!mounted) return;
    //   setIntAsync(USER_ID, res['user_id']);
    //   setStringAsync(FIRST_NAME, res['first_name']);
    //   setStringAsync(LAST_NAME, res['last_name']);
    //   setStringAsync(USER_EMAIL, res['user_email']);
    //   setStringAsync(USERNAME, res['user_nicename']);
    //   setStringAsync(TOKEN, res['token']);
    //   setStringAsync(AVATAR, res['avatar']);
    //   if (res['book_profile_image'] != null) {
    //     setStringAsync(PROFILE_IMAGE, res['book_profile_image']);
    //   }
    //   setStringAsync(USER_DISPLAY_NAME, res['user_display_name']);
    //   setBoolAsync(IS_LOGGED_IN, true);
    //   setBoolAsync(IS_SOCIAL_LOGIN, true);
    //   setState(() {
    //     isLoading = false;
    //   });
    //   DashboardActivity().launch(context, isNewTask: true);
    // }).catchError((error) {
    //   log("Error" + error.toString());
    //   setState(() {
    //     isLoading = false;
    //   });
    //   toast(error.toString());
    // });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Form(
                key: formKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(spacing_standard_new),
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Image.asset(
                                  "assets/images/splash.png",
                                  width: 200,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Container(
                            child: Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  EditText(
                                    hintText: "First name",
                                    isPassword: false,
                                    mController: firstname,
                                    mKeyboardType: TextInputType.text,
                                    validator: (String? s) {
                                      if (s!.trim().isEmpty)
                                        return "First name" +
                                            " " +
                                            "field require";
                                      if (s.contains(RegExp(
                                          r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]')))
                                        return "Error format";
                                      return null;
                                    },
                                  ),
                                  SizedBox(height: 14),
                                  EditText(
                                    hintText: "Last name",
                                    isPassword: false,
                                    mController: lastname,
                                    mKeyboardType: TextInputType.text,
                                    validator: (String? s) {
                                      if (s!.trim().isEmpty)
                                        return "Last name" +
                                            " " +
                                            "field require";
                                      if (s.contains(RegExp(
                                          r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]')))
                                        return "Error format";
                                      return null;
                                    },
                                  ),
                                  SizedBox(height: 14),
                                  EditText(
                                    hintText: "Username",
                                    isPassword: false,
                                    mController: username,
                                    mKeyboardType: TextInputType.text,
                                    validator: (String? s) {
                                      if (s!.trim().isEmpty)
                                        return "Username" +
                                            " " +
                                            "field require";
                                      if (s.contains(RegExp(
                                          r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]')))
                                        return "Error format";
                                      return null;
                                    },
                                  ),
                                  SizedBox(height: 14),
                                  EditText(
                                    hintText: "Email",
                                    isPassword: false,
                                    mController: usernameCont,
                                    mKeyboardType: TextInputType.emailAddress,
                                    validator: (String? s) {
                                      if (s!.trim().isEmpty)
                                        return "Email is require";
                                      if (!EmailValidator.validate(s)) {
                                        return "Email is invalid";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(height: 14),
                                  EditText(
                                    hintText: "Password",
                                    isPassword: true,
                                    mController: passwordCont,
                                    isSecure: true,
                                    validator: (String? s) {
                                      if (s!.trim().isEmpty)
                                        return "Password is require";
                                      if (s.length <= 5)
                                        return "Minimum length is 6";
                                      return null;
                                    },
                                  ),
                                  SizedBox(height: 14),
                                  EditText(
                                    hintText: "Enter Re-Enter Password*",
                                    isPassword: true,
                                    mController: confirmPasswordCont,
                                    isSecure: true,
                                    validator: (String? s) {
                                      if (s!.trim().isEmpty)
                                        return "Re-Enter Password is require";
                                      if (confirmPasswordCont.text !=
                                          passwordCont.text)
                                        return "Password not match";
                                      return null;
                                    },
                                  ),
                                  SizedBox(height: 14.0),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 14),
                        AppBtn(
                          value: "Sign Up",
                          onPressed: () {
                            hideKeyboard(context);
                            var request = {
                              "first_name": "${firstname.text}",
                              "last_name": "${lastname.text}",
                              "username": "${username.text}",
                              "user_email": "${usernameCont.text}",
                              "password": passwordCont.text,
                            };
                            if (!mounted) return;
                            setState(() {
                              if (_formKey.currentState!.validate()) {
                                setState(() {
                                  isLoading = true;
                                });
                                registerUser(request, context);
                              } else {
                                setState(() {
                                  isLoading = false;
                                  autoValidate = true;
                                });
                              }
                            });
                          },
                        ).paddingOnly(left: 20, right: 20),
                        SizedBox(
                          height: 20,
                        ),
                        Wrap(
                          children: [
                            Text("Already have an account?",
                                style: primaryTextStyle(
                                  size: 18,
                                  color: Colors.white,
                                )),
                            Container(
                                margin: EdgeInsets.only(left: 4),
                                child: GestureDetector(
                                    child: Text("Sign In",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: primaryColor,
                                        )),
                                    onTap: () {
                                      Navigator.pop(context);
                                    })),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            isLoading
                ? Container(
                    child: CircularProgressIndicator(),
                    alignment: Alignment.center,
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
