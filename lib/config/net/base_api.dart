import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:musilier/config/storage_manager.dart';
import 'package:musilier/service/login_service.dart';
import 'package:musilier/utils/Constant.dart';
import 'api.dart';

final Http http = Http();

class Http extends BaseHttp {
  @override
  void init() {
    options.baseUrl = 'http://192.168.1.9:8080';
    // options.baseUrl = 'https://ancient-cove-14969.herokuapp.com';
    interceptors..add(ApiInterceptor())

        /*
      ..add(CookieManager(
          PersistCookieJar(dir: StorageManager.temporaryDirectory.path)))*/
        ;
  }
}

class ApiInterceptor extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    debugPrint('---api-request--->url--> ${options.baseUrl}${options.path}' +
        ' queryParameters: ${options.queryParameters}' +
        ' data: ${options.data}');
    String token = StorageManager.sharedPreferences!.getString(TOKEN) ?? '';
    options.headers["Authorization"] = "Bearer " + token;
    return super.onRequest(options, handler);
  }

  @override
  onResponse(Response response, ResponseInterceptorHandler handler) {
    ResponseData respData = ResponseData.fromJson(response.data);
    if (respData.success) {
      response.data = respData.data;
      return super.onResponse(response, handler);
    } else {
      debugPrint("error response${respData.code} ${respData.success}");
      if (respData.code == -1001) {
        // StorageManager.localStorage.deleteItem(UserModel.keyUser);
        throw const UnAuthorizedException();
      } else {
        throw NotSuccessException.fromRespData(respData);
      }
    }
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = new Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return http.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    debugPrint("error ${err.type}");
    if (err.response?.statusCode == 403) {
      await LoginRepository.refreshToken();
      handler.resolve(await _retry(err.requestOptions));
    }
    return super.onError(err, handler);
  }
}

class ResponseData extends BaseResponseData {
  bool get success => 200 == code;

  ResponseData.fromJson(Map<String, dynamic> json) {
    code = json['code'] as int;
    error = json['error'];
    data = json['data'];
  }
}
