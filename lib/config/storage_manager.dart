import 'dart:io';

import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';

class StorageManager {
  static SharedPreferences? sharedPreferences;

  static Directory? temporaryDirectory;


  static LocalStorage? localStorage;

  static Future init() async {
    temporaryDirectory = await getTemporaryDirectory();
    sharedPreferences = await SharedPreferences.getInstance();
    localStorage = LocalStorage('LocalStorage');
    if (localStorage != null)
    await localStorage?.ready;
  }
}
